<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\PDO;

/**
 * Description of Command
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class Command
{
    protected static $traceConf = [
        'dat_ins__' => ['type'=>'datetime'],
        'usr_ins__' => ['type'=>'uuid'],
        'dat_upd__' => ['type'=>'datetime'],
        'usr_upd__' => ['type'=>'uuid'],
        'dat_del__' => ['type'=>'datetime'],
        'usr_del__' => ['type'=>'uuid'],
    ];
    protected static $operConf = [
        'id__' => ['type'=>'uuid', 'pkey'=>'1'],
        'cc__' => ['type'=>'integer'],
        'is_del__' => ['type'=>'integer', 'size'=>'1'],
        'dat_opr__' => ['type'=>'datetime'],
        'usr_opr__' => ['type'=>'uuid'],
    ];
    
    protected $lib;
    
    public function __construct(PDOLib $lib)
    {
        $this->lib = $lib;
    }
    
    public function exec()
    {
        foreach($this->make() as $sql) {
            if ($sql) {
                $this->lib->getManager()->exec($sql);
            }
        }
    }
    
    abstract public function make();
}
