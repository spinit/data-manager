<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\PDO\Managers\Sqlite;

use Spinit\Datastruct\DataStructInterface;
use Spinit\Datastruct\Field;
use Spinit\Datastruct\Index;
use Spinit\Datamanager\PDO\PDOLib;
use Spinit\Datamanager\DatastructConverter\Sqlite2DataStruct;
use Spinit\Util;

use Spinit\Datamanager\PDO\Command\CreateTable;

use Spinit\Util\Error\NotFoundException;

/**
 * Description of MysqlManager
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Lib extends PDOLib
{
    
    public function checkTable($table)
    {
        try {
            $par = ['name' => $table];
            return $this->getManager()
                        ->load("SELECT name FROM sqlite_master WHERE type = 'table' and name = :name", $par)
                        ->first('name') == $table;
        } catch (NotFoundException $e) {
            return false;
        }
    }
    
    protected function getDataStructList()
    {
        return new Sqlite2DataStruct($this->getManager()->getPDO());
    }
    
    protected function getTypeField($struct, Field $field, $trace, $addCommand)
    {
        switch($field->get('type')) {
            case 'increment':
                if (!$trace) {
                    return 'integer';
                } else {
                    return 'integer';
                }
            case 'uuid':
                return 'binary(16)';
        }
        return parent::getTypeField($struct, $field, $trace, $addCommand);
    }
    
    public function createTableIndex($resource, $name, $fieldList = array(), $type = 'BTREE')
    {
        if (!$name) {
            $name = $resource.'_idx_'.rand(1000,9999);
        }
        return "CREATE INDEX {$name} ON {$resource} (".implode(', ', $fieldList).")";
    }

    public function updateTableIndex(DataStructInterface $struct, $name, Index $index) {
        
    }
    
    public function addTableField($struct, Field $field, $trace, $addCommand)
    {
        $this->updateTableField($struct, $field, $trace, $addCommand);
    }
    public function updateTableField($struct, Field $field, $trace, $addCommand)
    {
        $addCommand("BEGIN TRANSACTION;");
        $addCommand("ALTER TABLE {$struct->getName()} RENAME TO __{$struct->getName()};");
        $struct->addField($field);
        
        $create = new CreateTable($this, $struct);
        foreach($create->make() as $sql) {
            $addCommand($sql);
        }
        $ds = $this->getDataStruct("{$struct->getName()}");
        $fields = implode(', ', array_keys($ds->getFieldList()));
        $addCommand("INSERT INTO {$struct->getName()} ({$fields}) SELECT {$fields} FROM __{$struct->getName()};");
        $addCommand("DROP TABLE __{$struct->getName()};");
        $addCommand("COMMIT;");
        return '';
    }
    public function unhex($value)
    {
        return 'x'.$this->getManager()->quote($value);
    }
}