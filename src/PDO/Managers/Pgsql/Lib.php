<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager\PDO\Managers\Pgsql;

use Spinit\Datastruct\DataStructInterface;
use Spinit\Datastruct\Field;
use Spinit\Datastruct\Index;
use Spinit\Datamanager\PDO\PDOLib;
use Spinit\Datamanager\DatastructConverter\Pgsql2DataStruct;
use Spinit\Util;

/**
 * Description of MysqlManager
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Lib extends PDOLib
{
    
    public function checkTable($table)
    {
        try {
            $par = ['table_schema'=>$this->getManager()->getSchema(), 'table_name'=>$table];
            $first = $this->getManager()->find('information_schema.tables', 'table_name', $par)->first();
            return $first['table_name'] == $table;
        } catch (NotFoundException $e) {
            return false;
        }
    }
    
    protected function getDataStructList()
    {
        return new Pgsql2DataStruct($this->getManager()->getPDO());
    }
    
    protected function getTypeField($struct, Field $field, $trace, $addCommand, $isUpdate = false)
    {
        throw new \Exception('Da implementare');
        switch($field->get('type')) {
            case 'increment':
                if (!$trace) {
                    if ($field->get('incval', '') and $addCommand and $struct and !$isUpdate) {
                        $addCommand('ALTER TABLE '.$struct->getName().' AUTO_INCREMENT='.$field->get('incval'));
                    }
                    return 'bigint unsigned auto_increment';
                } else {
                    return 'bigint unsigned';
                }
            case 'uuid':
                return 'binary(16)';
        }
        if (!$trace) {
            if ($field->get('autoinc', '') and $struct) {
                if ($field->get('incval', '') and $addCommand and $struct and !$isUpdate) {
                    $addCommand('ALTER TABLE '.$struct->getName().' AUTO_INCREMENT='.$field->get('incval'));
                }
                return parent::getTypeField($struct, $field, $trace, $addCommand, $isUpdate).' auto_increment';
            }
        }
        return parent::getTypeField($struct, $field, $trace, $addCommand, $isUpdate);
    }
    public function createTableIndex($resource, $name, $fieldList = array(), $type = 'BTREE')
    {
        throw new Exception('Da implementare');
        if (!$name) {
            $name = $resource.'_idx_'.rand(1000,9999);
        }
        switch($type) {
            case 'FULLTEXT':
                $cmd = "CREATE {$type} INDEX {$name} ON {$resource} (".implode(', ', $fieldList).")";
                break;
            default:
                $cmd = "CREATE INDEX {$name} USING {$type} ON {$resource} (".implode(', ', $fieldList).")";
                break;
        }
        return $cmd;
    }

    public function updateTableIndex(DataStructInterface $struct, $name, Index $index) {
        
    }
}
