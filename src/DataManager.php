<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datamanager;

use Spinit\Util;
use Spinit\Datastruct\DataStructInterface;
use Spinit\UUIDO;

/**
 * Description of DataManager
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class DataManager implements DataManagerInterface, \Serializable
{
    private $info;
    private $param;
    private $schema = '';
    /**
     *
     * @var Spinit\UUIDO\Maker
     */
    private $counter;
    
    use \Spinit\Util\TriggerTrait;
    
    abstract public function connect();
    
    abstract public function getCommandList($reset = false);
    abstract public function getCommandLast();
    
    abstract public function getDataStruct($name);
    abstract public function getLastID();
        
    public function serialize()
    {
        $data = array('info' => $this->info, 'param'=>$this->param, 'schema'=>$this->schema, 'counter'=>serialize($this->counter));
        return serialize($data);
    }
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        if (is_array($data)) {
            $this->info = $data['info'];
            $this->param = $data['param'];
            $this->schema = $data['schema'];
            $this->counter = unserialize($data['counter']);
        }
    }
    public function setSchema($name)
    {
        $this->schema = $name;
    }
    public function getSchema()
    {
        return $this->schema;
    }
    public function __construct($info, $params = [])
    {
        $this->info = Util\asArray($info, ':');
        $this->param = $params;
        $this->setCounter(new UUIDO\Maker(UUIDO\randHexToday()));
    }
    
    public function getInfo()
    {
        $args = func_get_args();
        if (!count($args)) {
            return $this->info;
        }
        return Util\arrayGet($this->info, $args[0]);
    }
    
    public function setParam($name, $value)
    {
        $this->param[$name] = $value;
        return $this;
    }
    public function getParam($name, $default = '')
    {
        return Util\arrayGet($this->param, $name, $default);
    }
    
    /**
     * Imposta il counter che genererà gli ID binari
     * 
     * @param \Spinit\UUIDO\Maker $counter
     */
    public function setCounter(UUIDO\Maker $counter)
    {
        $this->counter = $counter;
    }
    
    /**
     * 
     * @return UUIDO\UUIDO;

     */
    public function getCounter()
    {
        return $this->counter;
    }
    
    /**
     * Estrae da $info il paramentro richiesto. Se $info è una stringa allora lo restituisce solo se la proprietà
     * richiesta è "name"
     * 
     * @param type $info
     * @param type $name
     * @return type
     */
    protected function getResourceInfo($info, $param)
    {
        if (is_string($info) and $param == 'name') {
            return $info;
        }
        return Util\arrayGet($info, $param);
    }
}
