<?php
namespace Spinit\Datamanager\Test\Functional\PDOManagerUuido;

use Spinit\Datamanager\DataManagerFactory;
use PHPUnit\Framework\TestCase;
use Spinit\Datastruct\DataStruct;
use Spinit\Datastruct\Field;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDOManagerTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class PDOManagerUuidoTest extends TestCase
{
    /**
     *
     * @var PDOManager
     */
    private $obj = null;
    public function setUp()
    {
        $this->obj = DataManagerFactory::getDataManager(CONNECTION_STRING);
    }
    public function testUuidField()
    {
        $ds = new DataStruct('prova_uid');
        $ds->addField(new Field('id'))->set('type', 'uuid');
        $ds->addPkeyField('id');
        $this->obj->align($ds);
        
        $grp = $this->obj->getDataStruct('prova_uid');
        $this->assertNotNull($grp);
        $this->assertEquals('uuid', $grp->getField('id')->get('type'));
    }
    public function testInsertUuid()
    {
        $key = (string) $this->obj->getCounter()->next();
        $key2 = $this->obj->insert('prova_uid',['id'=> $key]);
        $this->assertEquals(['id'=>$key], $key2);
        $rec = $this->obj->find('prova_uid','id', $key)->first();
        $this->assertEquals($key, $rec['id']);
    }
}
