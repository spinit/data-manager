<?php
namespace Spinit\Datamanager\Test\Functional\PDOManager;

use Spinit\Datamanager\DataManagerFactory;
use PHPUnit\Framework\TestCase;
use Spinit\Datamanager\PDO\PDOManager;
use Spinit\Datastruct\DataStruct;
use Spinit\Datastruct\Field;
use Spinit\Datamanager\DatastructConverter\Sqlite2DataStruct;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDOManagerTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class PDOManagerSqliteTest extends TestCase
{
    /**
     *
     * @var PDOManager
     */
    private $obj = null;
    public function setUp()
    {
        $this->obj = DataManagerFactory::getDataManager('sqlite::memory:');
    }
    public function testConnection()
    {
        $this->obj->connect();
        $this->obj->exec('DROP TABLE IF EXISTS prova');
        $this->obj->exec('DROP TABLE IF EXISTS prova__');
        $this->assertFalse($this->obj->getLib()->checkTable('prova'));
    }
    public function testCreateTable()
    {
        $ds = new DataStruct('prova');
        $ds->addField(new Field('id'))->set('type', 'varchar')->set('size', '20');
        $this->obj->align($ds);
        $this->assertTrue($this->obj->getLib()->checkTable('prova'));
        $this->obj->insert('prova', ['id'=>'10']);
        $res = $this->obj->load('select * from prova')->first();
        $this->assertEquals(['id'=>'10'], $res);
        $this->obj->drop('prova');
        $this->assertFalse($this->obj->check('prova'));
    }
    public function testMysqlDump()
    {
        $dsList = new \Spinit\Datastruct\Converter\MysqldumpXml2DataStruct(__DIR__.'/xml/Test-D1.xml');
        $this->obj->setParam('xmlroot', __DIR__.'/xml');
        foreach($dsList->getDataStructList() as $ds) {
            $this->obj->align($ds);
        }
        $this->assertTrue($this->obj->getLib()->checkTable('osy_user'));
        
    }
    public function testAddColumn()
    {
        $dsUser = $this->obj->getDataStruct('osy_user');
        $this->assertEquals('20', $dsUser->getField('tel_2')->get('size'));
        
        $ds = new DataStruct('osy_user');
        $ds->addField(new Field('test_field'))->set('type', 'varchar')->set('size', '1');
        $ds->addField(new Field('tel_2'))->set('type', 'varchar')->set('size', '50');
        
        $this->obj->align($ds);
        
        $dsList = new Sqlite2DataStruct($this->obj->getPdo());
        
        $dsUser = $dsList->getDataStruct('osy_user');
        $this->assertEquals('50', $dsUser->getField('tel_2')->get('size'));
        $this->assertEquals('1', $dsUser->getField('test_field')->get('size'));
    }
    
    public function testAddRow()
    {
        $ds = new DataStruct('ttrace2');
        $ds->setParam('trace', 'trace');
        $pk = $ds->addField(new Field('id'))->set('type', 'increment');
        $ds->addPkeyField($pk->getName());
        $ds->addField(new Field('nme'))->set('type', 'varchar')->set('size', '50');
        $this->obj->align($ds);
        $this->obj->setParam('userTrace','12345678');
        
        $this->obj->insert(['name'=>'ttrace2', 'trace'=>'trace'], ['nme'=>'test']);        
        $ret = $this->obj->find('ttrace2', '*', $this->obj->getLastID())->first();
        $this->assertEquals('test', $ret['nme']);
        
        $this->obj->update(['name'=>'ttrace2', 'trace'=>'trace'], ['nme'=>'ok'], $ret['id']);
        $ret = $this->obj->find('ttrace2', 'id, nme', $ret['id'])->first();
        $this->assertEquals('ok', $ret['nme']);
        
        $this->obj->delete(['name'=>'ttrace2', 'trace'=>'trace'], $ret['id']);
        $ret = $this->obj->find('ttrace2', '*', $ret['id'])->first();
        $this->assertFalse($ret);
    }
    
}
