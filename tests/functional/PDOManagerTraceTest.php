<?php
namespace Spinit\Datamanager\Test\Functional\PDOManager;

use Spinit\Datamanager\DataManagerFactory;
use PHPUnit\Framework\TestCase;
use Spinit\Datamanager\PDO\PDOManager;
use Spinit\Datastruct\DataStruct;
use Spinit\Datastruct\Field;
use Spinit\Datamanager\DatastructConverter\Mysql2DataStruct;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDOManagerTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class PDOManagerTraceTest extends TestCase
{
    /**
     *
     * @var PDOManager
     */
    private $obj = null;
    public function setUp()
    {
        $this->obj = DataManagerFactory::getDataManager(CONNECTION_STRING);
    }
    
    public function testAddRow()
    {
        $ds = new DataStruct('ttrace2');
        $ds->setParam('trace', 'trace');
        $pk = $ds->addField(new Field('id'))->set('type', 'increment');
        $ds->addPkeyField($pk->getName());
        $ds->addField(new Field('nme'))->set('type', 'varchar')->set('size', '50');
        $this->obj->align($ds);
        $this->obj->setParam('userTrace','12345678');
        
        // ritorna un record
        $this->obj->insert(['name'=>'ttrace2', 'trace'=>'trace'], ['nme'=>'test']);        
        $ret = $this->obj->first('ttrace2', $this->obj->getLastID());
        $this->assertEquals('test', $ret['nme']);
        // ritorna un singolo valore
        $this->obj->update(['name'=>'ttrace2', 'trace'=>'trace'], ['nme'=>'ok'], $ret['id']);
        $val = $this->obj->first('ttrace2', $ret['id'], 'nme');
        $this->assertEquals('ok', $val['nme']);
        $this->obj->delete(['name'=>'ttrace2', 'trace'=>'trace'], $ret['id']);
        $ret = $this->obj->first('ttrace2', $ret['id']);
        $this->assertFalse($ret);
    }
    
    public function testDelSoft()
    {
        $this->obj->setParam('userTrace','8654321');
        
        // ritorna un record
        $this->obj->insert(['name'=>'ttrace2', 'trace'=>'trace'], ['nme'=>'da cancellare']);
        $ret = $this->obj->first('ttrace2', $this->obj->getLastID());
        $this->assertEquals('da cancellare', $ret['nme']);
        // ritorna un singolo valore
        $this->obj->update(['name'=>'ttrace2', 'trace'=>'trace'], ['nme'=>'modificato'], $ret['id']);
        $val = $this->obj->first('ttrace2', $ret['id'], 'nme');
        $this->assertEquals('modificato', $val['nme']);
        $this->obj->delete(['name'=>'ttrace2', 'trace'=>'trace'], $ret['id'], true);
        $ret = $this->obj->first('ttrace2', $ret['id']);
        $this->assertNotNull($ret['dat_del__']);
    }
    
}
