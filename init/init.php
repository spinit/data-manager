<?php
namespace Spinit\Datamanager;

DataManagerFactory::setDataManager('mysql, mysqli, mariadb', 'Spinit:Datamanager:PDO:Managers:Mysql:Manager');
DataManagerFactory::setDataManager('sqlite', 'Spinit:Datamanager:PDO:Managers:Sqlite:Manager');
DataManagerFactory::setDataManager('pgsql', 'Spinit:Datamanager:PDO:Managers:Pgsql:Manager');

DataManagerFactory::setTypeSerializer("mysql, mysqli, mariadb, sqlite, pgsql", "date, datetime", function($value) {
    $datatime = explode(' ', $value);
    $arData = explode('/', $datatime[0]);
    if (count($arData)>1) {
        $arData = array_reverse($arData);
        $datatime[0] = implode('-', $arData);
    }
    return implode(' ', $datatime);
});
